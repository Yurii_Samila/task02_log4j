package com.yurii.log4j;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
public class ExampleSMS {
  // Find your Account Sid and Token at twilio.com/user/account
  public static final String ACCOUNT_SID = "AC539f63c2a5b94626ee1fb56344b3d2f2";
  public static final String AUTH_TOKEN = "7ee729467ef4a3e4803582059f41af16";
  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message
        .creator(new PhoneNumber("+380633275156"), /*my phone number*/
            new PhoneNumber("+13217665519"), str) .create(); /*attached to me number*/
  }
}

